//
//  ListSwiftUiApp.swift
//  ListSwiftUi
//
//  Created by Irianda on 06/10/21.
//

import SwiftUI

@main
struct ListSwiftUiApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
