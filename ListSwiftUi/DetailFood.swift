//
//  DetailFood.swift
//  ListSwiftUi
//
//  Created by Irianda on 06/10/21.
//

import SwiftUI

struct DetailFood: View {
    var name : String = "Rendang"
    var description : String = "Enak banget"
    
    var body: some View {
        NavigationView{
            VStack{
                VStack(alignment:.leading){
                    VStack(alignment:.leading){
                        Text("Nama Food")
    
                        Text("Description Food")
                            .bold()
                            .font(.system(size: 20))
                    }
                }
                .frame(width: UIScreen.main.bounds.width - 40 , height: 100)
                .background(Color.yellow)
                Spacer()
            }
            .navigationTitle("Detail")
        }
    }
}

struct DetailFood_Previews: PreviewProvider {
    static var previews: some View {
        DetailFood()
    }
}
