//
//  LIstpoke.swift
//  ListSwiftUi
//
//  Created by Irianda on 06/10/21.
//

import SwiftUI

struct Listpoke: View {
    @ObservedObject var dataPoke = PokeList()
    var body: some View{
      NavigationView{
        List(dataPoke.data){ item in
          NavigationLink(destination: DetailFood()){
            HStack{
              
                Text(item.name)
                    .bold()
            }
          }
          
        }
        .navigationBarTitle(Text("List Pokemon"))
      }.navigationViewStyle(StackNavigationViewStyle())
      
    }
  }

struct LIstpoke_Previews: PreviewProvider {
    static var previews: some View {
        Listpoke()
    }
}
