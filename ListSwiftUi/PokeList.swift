//
//  PokeList.swift
//  ListSwiftUi
//
//  Created by Irianda on 06/10/21.
//

import Foundation
import SwiftyJSON


class PokeList: ObservableObject {
    @Published var data = [Foods]()
    
    
    init(){
        let url = "https://pokeapi.co/api/v2/pokemon/"
        
        let session = URLSession(configuration: .default)
        
        session.dataTask(with: URL(string: url)!){ (data, _, error)in
            //jika ada error
            if error != nil{
                print((error?.localizedDescription)!)
                return
            }
            
            let json = try! JSON (data:data!)
            let items = json["results"].array!
            
            for poke in items{
                let name = poke["name"].stringValue
               
                
                DispatchQueue.main.async {
                    self.data.append(Foods(name: name))
                }
            }
        }.resume()
        
    }
}

